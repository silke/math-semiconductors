# Mathematics for semiconductor devices
The mathematics can be found in the PDF document in the latest [Release][release].

Check the [builds section][builds] for a 'nightly' version.

## Compiling the document yourself
This document requires the `utwentetitle` package from the [LaTeX UTwente project][latex_utwente].

[release]:       https://git.snt.utwente.nl/silke/math-semiconductors/tags/v0.0.1
[builds]:        https://git.snt.utwente.nl/silke/math-semiconductors/builds/
[latex_utwente]: https://git.snt.utwente.nl/visual-identity/latex-utwente/
