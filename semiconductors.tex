\documentclass[11pt,a4paper,english]{article}
\usepackage{
	geometry,  % Page layout
	amssymb,   % Maths symbols
	commath,   % Maths commands
	esint,     % More integrals
	siunitx,   % SI Units
	babel,     % Language support
	caption,   % Improved captions
	xparse,    % LaTeX3 commands
	parskip,   % Use white space paragraph breaks
	booktabs,  % Table style improvements
	tabularx,  % Tab­u­lars with ad­justable-width columns
}
\usepackage[scale=2]{ccicons}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage[style,nomath,freefont]{utwentetitle}

\sisetup{
	inter-unit-product=\ensuremath{\cdot},
	math-ohm=\Omega,
	text-ohm=Ω,
}
\numberwithin{equation}{section}
\newcolumntype{L}{>{$}l<{$}}
\newcolumntype{C}{>{$}c<{$}}
\newcolumntype{P}{>{$}p{.3\textwidth}<{$}}

% Macros
\ExplSyntaxOn
\cs_set_eq:NN \macro \tl_gset:Nn
\ExplSyntaxOff

\DeclareDocumentCommand\sdref{m}{\emph{Semiconductor Devices Explained More} (2010, p. #1)}
\DeclareDocumentCommand\exp{m}{e^{#1}}
\DeclareMathOperator\var{var}

% Symbols
\macro\mcr{\\[1em]}
\macro\tcoll{\tau_{\mathrm{coll}}}
\macro\lcoll{\lambda_{\mathrm{coll}}}
\macro\varE{\mathcal{E}}

% Table spacing
\renewcommand{\arraystretch}{1.2}

% Title
\title{Mathematics}
\subtitle{for Semiconductor Devices}
\author{Silke Hofstra, Wouter Horlings}

\begin{document}
\maketitle
This document is meant to be a somewhat comprehensive summary of the most important laws,
equations and relations necessary for \emph{Semiconductor Devices}.

Everything marked with a * is included on the formula sheet provided during the exam.

\tableofcontents

\vfill

\begin{minipage}{.65\textwidth}
	This document is published under the
	\emph{Creative Commons Attribution-ShareAlike 4.0 International} licence.
	\texttt{https://creativecommons.org/licenses/by-sa/4.0/}
\end{minipage}
\hfill
\begin{minipage}{.25\textwidth}
	\raggedleft \ccbysa
\end{minipage}

\clearpage

\section{Symbols}

\subsection{Prior Knowledge}
\begin{tabularx}{\textwidth}{CcX}
	\toprule
	\text{Symbol} & Value/Unit & Description \\
	\midrule
	q		& \SI{1.6e-19}{\coulomb}		& Elementary charge* \\
	\sigma	& \si{\per\ohm\per\cm}			& Conductivity \\
	\rho	& \si{\ohm\cm}					& Resistivity \\
	J		& \si{\ampere\per\cm\squared}	& Current density \\
	v		& \si{\cm\per\second}			& Velocity \\
	T		& \si{\kelvin}					& Temperature \\
	\varE	& \si{\volt\per\cm}				& Electric field \\
\varepsilon & \si{\farad\per\cm}			& Permittivity \\
	\psi	& \si{\volt}					& Electric Potential \\
	V		& \si{\volt}					& Voltage \\
	\bottomrule
\end{tabularx}


\subsection{Semiconductor Physics and Technology}
\begin{tabularx}{\textwidth}{CcX}
	\toprule
	\text{Symbol} & Value/Unit & Description \\
	\midrule
	k		& \SI{1.38064852e-23}{\joule\per\kelvin}		& Boltzmann constant \\
			& \SI{8.6173324e-5}{\electronvolt\per\kelvin}	& \\
	n_i		& \si{cm^{-3}} 					& Intrinsic carrier concentration \\
	n		& \si{\per\cm\cubed}			& Number of electrons in the conduction band \\
	p		& \si{\per\cm\cubed}			& Number of unoccupied states in the valence band \\
	\lambda & \si{\cm}						& Collision distance \\
	\mu		& \si{\cm\squared\per\volt\per\second}	& Mobility \\
	\tau	& \si{\second}					& Lifetime \\
	E_I, E_{FI} & \si{\electronvolt} 		& Intrinsic Fermi level \\
	E_F		& \si{\electronvolt}			& Fermi level \\
	E_C		& \si{\electronvolt}			& Conduction band \\
	E_V		& \si{\electronvolt}			& Valence band \\
	E_D		& \si{\electronvolt}			& Donor level \\
	E_A		& \si{\electronvolt}			& Acceptor level \\
	N_x		& \si{\per\cm\cubed}			& Effective density of states at $E_x,\;x\in\{C, V\}$ \\
	N_D		& \si{\per\cm\cubed}			& Number of donors \\
	N_A		& \si{\per\cm\cubed}			& Number of acceptors \\
	N_D^+	& \si{\per\cm\cubed}			& Number of ionised donors \\
	N_A^-	& \si{\per\cm\cubed}			& Number of ionised acceptors \\
	D_x		& \si{\cm\squared\per\second}	& Diffusion constant of carrier type $x\in\{n, p\}$ \\
	L_x		& \si{\cm}						& Diffusion length of carrier type $x\in\{n, p\}$ \\
	u_T, \frac{kT}{q} & \SI{0.025}{\volt}	& Thermal voltage equivalent (room temperature)* \\
	G		& \si{\per\cm\cubed\per\second} & Generation rate \\
	R		& \si{\per\cm\cubed\per\second} & Recombination rate \\
	U		& \si{\per\cm\cubed\per\second} & Net effect of generation and recombination \\
	c_x		&								& Capture coefficient of carrier type $x\in\{n, p\}$ \\
	e		& & \\
	\bottomrule
\end{tabularx}

\subsection{Junction Diodes}

\begin{tabularx}{\textwidth}{CcX}
	\toprule
	\text{Symbol} & Value/Unit & Description \\
	\midrule
	V_{bi}	& \si{\volt}					& Built-in voltage \\
	\varepsilon_s, \varepsilon_{Si} & \SI{e-12}{\farad\per\cm}	& Dielectric constant (permittivity) of Silicon* \\
	\varepsilon_{ox} & \SI{3.5e-13}{\farad\per\cm}				& Dielectric constant (permittivity) of Silicon dioxide* \\
	W		& \si{\cm}						& Depletion layer thickness \\
	\bottomrule
\end{tabularx}

\subsection{Bipolar Junction Transistors}

\begin{tabularx}{\textwidth}{CcX}
	\toprule
	\text{Symbol} & Value/Unit & Description \\
	\beta_F &								& Current amplification factor \\
	\alpha_F, \gamma_F &					& Emitter efficiency \\
	\phi_B	& \si{\volt}					& Built-in potential of the depleted layer\\
	\midrule
	\bottomrule
\end{tabularx}

\subsection{MOS Transistors}

\begin{tabularx}{\textwidth}{CcX}
	\toprule
	\text{Symbol} & Value/Unit & Description \\
	\midrule
	\psi_{ox} & \si{\volt}					& Voltage drop across the oxide \\
	Q_{dep} & \si{\coulomb}					& Depletion charge \\
	D_p		& \si{\cm\squared\per\second}	& Hole diffusion constant \\
	D_n		& \si{\cm\squared\per\second}	& Electron diffusion constant \\
	\bottomrule
\end{tabularx}

\clearpage

\subsection{Default values*}
These values should be used if they are not specified in the question:

\begin{tabularx}{\textwidth}{CcX}
	\toprule
	\text{Symbol} & Value/Unit & Description \\
	\midrule
	n_i		& $\sqrt{2}\times$\SI{e10}{cm^{-3}} 		& Intrinsic carrier concentration \\
	D_n		& \SI{30}{\cm\squared\per\second}			& Electron diffusion constant \\
	D_p		& \SI{10}{\cm\squared\per\second}			& Hole diffusion constant \\
	\mu_n	& \SI{1200}{\cm\squared\per\volt\per\second}& Electron mobility \\
	\mu_p	& \SI{350}{\cm\squared\per\volt\per\second}	& Hole mobility \\
	\bottomrule
\end{tabularx}

\section{Semiconductor Physics and Technology}

Ohm's law:
\[ R = \frac{V}{I} \]

Conductivity:
\begin{align*}
\sigma &= \frac{1}{\rho} = \frac{L}{A \cdot R} \\
	&= q \cdot n \cdot \mu_n \\
	&= q \cdot n \cdot \mu_n + q \cdot p \mu_p \\
	&= \frac{I \cdot \ln(2)}{V\pi w} & \text{Wafer with: $s \ll w$} \\
	&= \frac{I}{V2\pi s} & \text{Wafer with: $s \gg w$}
\end{align*}

Coulomb force:
\[ F = -q \cdot \varE \]

Acceleration and velocity:
\begin{align*}
	a &= -\frac{q\cdot\varE}{m_0} \\
	v &= a \cdot \tau_{coll} \\
	  &= -\frac{q \cdot \varE \cdot \tau_{coll} }{m_0} \\
	v_d &= -\frac{q \cdot \varE \cdot \langle \tcoll \rangle}{m_0} \\
		&= -\mu_n \cdot \varE
\end{align*}

Current density:
\begin{align*}
	J &= \frac{I}{A} \\
	  &= -q \cdot n \cdot v_d \\
	  &= q \cdot n \cdot \mu_n \cdot \varE
\end{align*}

Electron mobility:
\[ \mu_n = \frac{\sigma}{q \cdot n} \]

Collision time and length:
\begin{align*}
	\tcoll &= \frac{m_0 \cdot \mu_n}{q} \\
	\lcoll &= v \cdot \tcoll
\end{align*}

Reaction rate:

\[ R_A \equiv \exp{-\frac{E_A}{k T}} \]

Fermi-Dirac distribution*:

\[ f(E) = \frac{1}{1 + \exp{\frac{E - E_F}{kT}}} \]

Density of states*:

\[ g(E) \sim 10^{47} \sqrt{E} \]

Carrier concentrations*:
\begin{align*}
	n &= N_C \exp{\frac{E_F - E_C}{kT}} = n_i \exp{\frac{E_F - E_{FI}}{kT}} \\
	p &= N_V \exp{\frac{E_V - E_F}{kT}} = n_i \exp{\frac{E_{FI} - E_F}{kT}}
\end{align*}

Electrostatic potential*:
\[ \psi = -\frac{E_{FI}}{q} \]

Fermi potential*:
\[ \varphi_F = -\frac{E_F}{q} \]

General formalism*:
\begin{align*}
	n &= \exp{\frac{\psi - \varphi_F}{u_T}} \\
	p &= \exp{\frac{\varphi_F - \psi}{u_T}}
\end{align*}

Current equations*:
\begin{align*}
	j_n &= q n \mu_n \varE + q D_n \od{n}{x} = n \mu_n \od{E_{FN}}{x} \\
	j_p &= q p \mu_p \varE - q D_p \od{p}{x} = p \mu_p \od{E_{FP}}{x}
\end{align*}

Einstein relation*:
\[ D = u_T \cdot \mu = \frac{kT}{q} \cdot \mu \]

Excess recombination rate (electrons)*:
\[ R = \widetilde{n} N_t c_n = \widetilde{n} N_t v_{th} \sigma_n = \frac{\widetilde{n}}{\tau_n} \]

Continuity equation (electrons)*:
\[ \od{\widetilde{n}}{t}
	= \frac{1}{q} \od{j_n}{x} - (R - G)
	= D_n \od[2]{\widetilde{n}}{x} - \frac{\widetilde{n}}{\tau_n} \]

Excess carrier diffusion (electrons)*:
\[ \widetilde{n}(x) = \widetilde{n}_0 \exp{-\frac{x}{L_n}} \]

Diffusion length*:
\[ L_n = \sqrt{D_n \tau_n} \]

Poisson's equation*:
\[ -\od[2]{\psi(x)}{x} = \od{\varE(x)}{x} = \frac{\rho(x)}{\varepsilon_s} \]

\section{Junction Diodes}

Build-in voltage:
\begin{align*}
	V_{bi}    &= \frac{1}{q} \left( E_{FI}(n)-E_{FI}(p) \right) \\
	\phi_{bi} &= - V_{bi} \\
	          &= \frac{kT}{q} \ln\left( \frac{N_DN_A}{n_i^2} \right) \\
	          &= u_T \ln\left( \frac{N_DN_A}{n_i^2} \right)
\end{align*}

Space charge:
\[ Q_{total} = \int\limits^{x_{dp}}_{-x_{dn}}\rho(x)dx = qN_Dx_{dn}-qN_Ax_{dp} = 0 \]

Depletion region dimensions:
\begin{align*}
	W &= x_{dn} + x_{dp} \\
	  &= \sqrt{ \frac{2 \varepsilon_s (N_A + N_D)}{q N_A N_D} ( \psi_{bi} - V_A )} \\
  \frac{x_{dn}}{x_{dp}} &= \frac{N_A}{N_D}
\end{align*}

\section{Bipolar Junction Transistors}

Emitter efficiency:
\[ \alpha_F = \gamma_F = \frac{\beta_F}{\beta_F + 1} \]

Built-in potential of the depleted layer in Si:
\[ \phi_B = \mu_T \cdot \ln(\frac{N_A}{n_i}) \]

\section{MOS Transistors}

Voltage drop across the oxide:
\[ \psi_{ox} = \frac{\sqrt{2 q \varepsilon_s N_A 2 \phi_B}}{C_{ox}} \]

Depletion charge:
\[ Q_{dep} = -q \cdot N_A \cdot W \]

Depletion layer thickness:
\[ W = \sqrt{\frac{2\varepsilon_s}{q N_A} \cdot 2 \phi_B} \]

\end{document}
